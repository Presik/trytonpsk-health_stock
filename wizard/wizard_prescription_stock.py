
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.model import ModelView
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.i18n import gettext
from trytond.exceptions import UserError


class CreatePrescriptionStockMoveInit(ModelView):
    'Create Prescription Stock Move Init'
    __name__ = 'health.prescription.stock.move.init'


class CreatePrescriptionStockMove(Wizard):
    'Create Prescription Stock Move'
    __name__ = 'health.prescription.stock.move.create'

    start = StateView('health.prescription.stock.move.init',
        'health_stock.view_create_prescription_stock_move', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create Stock Move', 'create_stock_move',
                'tryton-ok', True),
        ])
    create_stock_move = StateTransition()

    def transition_create_stock_move(self):
        pool = Pool()
        StockMove = pool.get('stock.move')
        Prescription = pool.get('health.prescription.order')

        moves = []
        prescriptions = Prescription.browse(Transaction().context.get(
            'active_ids'))
        for prescription in prescriptions:

            if prescription.moves:
                raise UserError(gettext('health_stock.msg_stock_move_exists'))

            if not prescription.pharmacy:
                raise UserError(gettext('health_stock.msg_no_pharmacy_selected'))

            from_location = prescription.pharmacy.warehouse
            if from_location.type == 'warehouse':
                from_location = from_location.storage_location
            to_location = prescription.patient.name.customer_location

            for line in prescription.prescription_line:
                move = StockMove()
                move.origin = prescription
                move.from_location = from_location
                move.to_location = to_location
                move.product = line.medicament.name
                move.unit_price = line.medicament.name.list_price
                move.quantity = line.quantity
                move.uom = line.medicament.name.default_uom
                moves.append(move)
        StockMove.save(moves)
        StockMove.do(moves)
        return 'end'

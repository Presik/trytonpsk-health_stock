from trytond.pool import Pool
from . import health_stock
from .wizard import wizard_prescription_stock, wizard_vaccination_stock


def register():
    Pool.register(
        health_stock.Party,
        health_stock.Lot,
        health_stock.Move,
        health_stock.PatientAmbulatoryCare,
        health_stock.PatientAmbulatoryCareMedicament,
        health_stock.PatientAmbulatoryCareMedicalSupply,
        health_stock.PatientRounding,
        health_stock.PatientRoundingMedicament,
        health_stock.PatientRoundingMedicalSupply,
        health_stock.PatientPrescriptionOrder,
        health_stock.PatientVaccination,
        health_stock.ShipmentInternal,
        wizard_prescription_stock.CreatePrescriptionStockMoveInit,
        wizard_vaccination_stock.CreateVaccinationStockMoveInit,
        module='health_stock', type_='model')
    Pool.register(
        wizard_prescription_stock.CreatePrescriptionStockMove,
        wizard_vaccination_stock.CreateVaccinationStockMove,
        health_stock.CreateInpatientShipment,
        health_stock.CreateStockShipment,
        module='health_stock', type_='wizard')
